package observer;

import client.SMSSystem;

public class Subscriber implements Observer {
    protected String name;
    protected String phoneNumber;
    public Subscriber(String name) {
        this.name = name;
    }

    public Subscriber(String name, String phoneNumber) {
        this(name);
        this.phoneNumber = phoneNumber;
    }
    @Override
    public void notification(String storeNameBoost, String news) {
        System.out.printf("\n%s received news \n %s \n NEWS: %s\n\n", this.name, storeNameBoost, news);
        if (this.phoneNumber != null) {
            if (this.phoneNumber.length() != 12) {
                System.out.println("Error: SMS can not be sent. Phone number must be 12 digits");
            } else {
                if (this.phoneNumber.startsWith("233")) {
                    String messageToSend = "Hi " +  this.name + ",\n" + news + "\n" + storeNameBoost;
                    //SM.SSystem.sendSMS(this.phoneNumber, messageToSend);
                } else {
                    System.out.println("Error: SMS can not be sent. Phone number must start with 233");
                }
            }
        }
    }

    public String getName() {
        return name;
    }
}
