package observer;

public interface Observer {
    void notification(String handle, String news);
    String getName();
}
