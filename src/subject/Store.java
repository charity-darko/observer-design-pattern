package subject;

import observer.Observer;

import java.util.ArrayList;
import java.util.List;

public class Store implements Subject {

    protected List<Observer> observers = new ArrayList<Observer>();
    protected String name;
    protected String storeNameBoost;

    public Store(String name) {
        super();
        this.name = name;
        this.storeNameBoost = "FROM: " + name;
    }

    public void sendNews(String news) {
        String text = "\n" + name + " just sent, News: " + news;
        System.out.println(text);
        for(int i = 0; i < text.length(); ++i) System.out.print("-");
        notifySubscribers(news);
    }

    @Override
    public synchronized void addSubscriber(Observer observer) {
        observers.add(observer);
    }
    @Override
    public synchronized void removeSubscriber(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifySubscribers(String news) {
        observers.forEach(observer -> observer.notification(storeNameBoost, news));
    }

    public void getObservers() {
        observers.forEach(observer -> System.out.println(observer.getName()));
    }

    public int getNumberOfObservers() {
        return observers.size();
    }

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

}