package client;

import com.sinch.xms.*;
import com.sinch.xms.api.*;

public class SMSSystem {
    private static final String SERVICE_PLAN_ID = Config.SMS_SERVICE_PLAN_ID.toString();
    private static final String TOKEN = Config.SMS_TOKEN.toString();
    private static ApiConnection conn;
    public static boolean sendSMS(String phoneNumber, String news) {
        String SENDER = Config.SMS_SENDER.toString();
        String[] RECIPIENTS = { phoneNumber };

        ApiConnection conn = ApiConnection
                .builder()
                .servicePlanId(SERVICE_PLAN_ID)
                .token(TOKEN)
                .start();
        MtBatchTextSmsCreate message = SinchSMSApi
                .batchTextSms()
                .sender(SENDER)
                .addRecipient(RECIPIENTS)
                .body(news)
                .build();
        try {
            // if there is something wrong with the batch
            // it will be exposed in APIError
            MtBatchTextSmsResult batch = conn.createBatch(message);
            //System.out.println(batch.id());
        } catch (Exception e) {
            //System.out.println(e.getMessage());
        }
        //System.out.println("you sent:" + message.body());
        return true;
    }
}