package test.subject;
import observer.Subscriber;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import subject.Store;

public class StoreTest {

    @Test
    public void addSubscriberTest() {
        Store vestir_mart = new Store("Vestir Mart");
        Subscriber vm_s1 = new Subscriber("Isaac", "233543842488");
        Subscriber vm_s2 = new Subscriber("Alice");

        vestir_mart.addSubscriber(vm_s1);
        vestir_mart.addSubscriber(vm_s2);

        int result = vestir_mart.getNumberOfObservers();

        Assertions.assertEquals(2, result);
    }

    @Test
    public void removeSubscriberTest() {
        Store mina_store = new Store("Mina Store");
        Subscriber ms_s1 = new Subscriber("Florencia", "233543842488");
        Subscriber ms_s2 = new Subscriber("Adobea");

        mina_store.addSubscriber(ms_s1);
        mina_store.addSubscriber(ms_s2);

        mina_store.removeSubscriber(ms_s1);

        int result = mina_store.getNumberOfObservers();

        Assertions.assertEquals(1, result);
    }
}
