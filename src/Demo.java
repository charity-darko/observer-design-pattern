import observer.Subscriber;
import subject.Store;

import java.util.List;

public class Demo {
    public static void main(String args[]) {

        // create a new store
        Store woyram_joint = new Store("Woyram's Joint");

        // create new subscribers
        Subscriber s1 = new Subscriber("Viki");
        Subscriber s2 = new Subscriber("Rachel");
        Subscriber s3 = new Subscriber("Felix", "233111111111");

        // add subscribers to a particular store
        woyram_joint.addSubscriber(s1);
        woyram_joint.addSubscriber(s2);
        woyram_joint.addSubscriber(s3);

        // send news to subscribed members
        woyram_joint.sendNews("I am in now.");

        // remove subscriber
        woyram_joint.removeSubscriber(s2);

        // send news to subscribed members to ensure a member has been removed
        woyram_joint.sendNews("The place is less crowded.");

        //woyram_joint.getObservers();

        /*
        // create a new store
        Store vestir_mart = new Store("Vestir Mart");

        // create new subscribers
        Subscriber vm_s1 = new Subscriber("Isaac", "233512342222");
        Subscriber vm_s2 = new Subscriber("Alice");

        // add subscribers to a particular store
        vestir_mart.addSubscriber(vm_s1);
        vestir_mart.addSubscriber(vm_s2);

        // send news to subscribed members
        vestir_mart.sendNews("2 in 1 keyboard combo with LED backlit going for GHS 95.00");
        */
    }
}
